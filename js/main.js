$(document).ready(function(){

        $(".cross_ic").click(function(){
           $(".header_top").hide(); 
        })

        /*
        =========================================================================================
        5.  BANNER SLIER
        =========================================================================================
        */

        var testimonail_slider = jQuery("#testimonail_slider");
        testimonail_slider.owlCarousel({
            loop: true,
            margin: 0,
            lazyLoad:true,
            center: true,
            smartSpeed: 1500,                
            autoplay:false,
            nav: false,
            //navText: ["<i class="material-icons"></i>","<i class="material-icons"></i>"],
            dots:true,  
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 1
                },
                768: {
                    items: 1
                },
                1200: {
                    items: 1
                }
            }
        });


        /*
        =========================================================================================
        5.  BANNER SLIER
        =========================================================================================
        */

        var brand_slider = jQuery("#brand_slider");
        brand_slider.owlCarousel({
            loop: false,
            margin: 0,
            smartSpeed: 1500,                
            autoplay:false,
            nav: true,
            dots:false,
            navText: ["<img src=images/arrow-left.png>","<img src=images/arrow-right.png>"] ,
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 1
                },
                768: {
                    items: 2
                },
                1200: {
                    items: 4
                }
            }
        });

        /*
        =========================================================================================
        5.  BANNER SLIER
        =========================================================================================
        */

        var restructure_slider = jQuery("#restructure_slider");
        restructure_slider.owlCarousel({
            loop: false,
            margin: 0,
            smartSpeed: 1500,                
            autoplay:false,
            nav: true,
            dots:false,
            navText: ["<img src=images/arrow-left.png>","<img src=images/arrow-right.png>"] ,
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 1
                },
                768: {
                    items: 1
                },
                1200: {
                    items: 1
                }
            }
        });

        /*
        =========================================================================================
        5.  BANNER SLIER
        =========================================================================================
        */

        var recent_renovations_slider = jQuery("#recent_renovations_slider");
        recent_renovations_slider.owlCarousel({
            loop: false,
            margin: 0,
            smartSpeed: 1500,                
            autoplay:false,
            nav: true,
            dots:false,
            navText: ["<img src=images/arrow-left.png>","<img src=images/arrow-right.png>"] ,
            responsive: {
                    0: {
                        items: 1
                    },
                    400: {
                        items: 1
                    },
                    768: {
                        items: 1
                    },
                    1200: {
                        items: 1
                    }
                }
            });
        /*
        =========================================================================================
        5.  BANNER SLIER
        =========================================================================================
        */

        var recent_renovations_slider2 = jQuery("#recent_renovations_slider2");
        recent_renovations_slider2.owlCarousel({
            loop: false,
            margin: 0,
            smartSpeed: 1500,                
            autoplay:false,
            nav: true,
            dots:true,
            navText: ["<img src=images/arrow-left.png>","<img src=images/arrow-right.png>"] ,
            responsive: {
                    0: {
                        items: 1
                    },
                    400: {
                        items: 1
                    },
                    768: {
                        items: 1
                    },
                    1200: {
                        items: 1
                    }
                }
            });

        /*
        =========================================================================================
        5.  BANNER SLIER
        =========================================================================================
        */

        var single_page_slider = jQuery("#single_page_slider");
        single_page_slider.owlCarousel({
            loop: false,
            margin: 0,
            smartSpeed: 1500,                
            autoplay:false,
            touchDrag: false,
            mouseDrag: false,
            pullDrag: false,
            freeDrag: false,
            nav: true,
            dots:true,
            navText: ["<img src=images/arrow-left.png>","<img src=images/arrow-right.png>"] ,
            responsive: {
                    0: {
                        items: 1
                    },
                    400: {
                        items: 1
                    },
                    768: {
                        items: 1
                    },
                    1200: {
                        items: 1
                    }
                }
            }).on("dragged.owl.carousel", function (event) {
        console.log('event : ',event.relatedTarget['_drag']['direction'])
    });

            

            $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4'
            });

        $('input:checkbox').change(function(){
            if($(this).is(":checked")) {
                $('.banner_checkbox label').addClass("showshawdow");
            } else {
                $('.banner_checkbox label').removeClass("showshawdow");
            }
        });
        
        initComparisons();
});


